'use strict';

var Printer = require('./printer');

var printer = new Printer({
	port: 'COM39'
});

async function testPrinter() {
	await printer.connect();
	/*
	// reset defaults
	await printer.resetDefaults();
	await printer.printDiagnostics();

	// set density (0-15)
	await printer.setDensity(8);

	// set codepage (0-2)
	await printer.setCodePage(0);

	// set height (42 mm * 8 = 336 dots) and space (3mm * 8 = 24 dots)
	await printer.setPageHeight(336, 24);

	// set width (max, in dots)
	await printer.setPageWidth(384);

	// set offset (x,y)
	await printer.setOffset(0, 0);

	// set print direction
	await printer.setFlipped(false);

	// print diag info
	await printer.printDiagnostics();
	*/
	// print EAN-8 barcode
	// draw it in buffer
	await printer.generateBarcode(24, 24, 0, 'E80', 5, 10, 240, true, '0000001');
	// actually print it
	await printer.printLabels();
	
	// bye bye
	await printer.disconnect();
}

testPrinter();
