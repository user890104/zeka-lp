'use strict';

const asyncSerialPort = require('./async-serialport')

module.exports = function(options) {
	const port = new asyncSerialPort(options);
	const connect = port.open;
	const disconnect = port.close;
	
	async function send(data) {
		return port.write(data).then(() => port.drain());
	}
	
	async function sendCommand(command) {
		return send(command + '\n');
	}
	
	function parseInteger(arg, min, max, defaultValue) {
		if (arg === undefined) {
			if (defaultValue !== undefined) {
				return defaultValue;
			}
			
			return undefined;
		}
		
		arg = parseInt(arg);
		
		if (min !== undefined && arg < min) {
			throw new Exception('Argument (' + arg + ') is smaller than ' + min);
		}
		
		if (max !== undefined && arg > max) {
			throw new Exception('Argument (' + arg + ') is bigger than ' + max);
		}
		
		return arg;
	}
	
	function quoteString(arg, defaultValue) {
		if (arg === undefined) {
			if (defaultValue !== undefined) {
				return defaultValue;
			}
			
			return undefined;
		}
		
		return '"' + arg + '"';
	}
	
	function makeParams() {
		return Array.from(arguments).map(function(arg) {
			return arg === undefined ? undefined : arg.toString();
		}).filter(function(arg) {
			return arg !== undefined;
		}).join(',');
	}
	
	async function setDensity(density) {
		density = parseInteger(density, 0, 15);
		return sendCommand('D' + makeParams(density));
	}
	
	async function setCodePage(cp) {
		cp = parseInteger(cp, 0, 2);
		return sendCommand('I' + makeParams(cp));
	}
	
	async function setPageHeight(height, gapHeight) {
		height = parseInteger(height, 0, 1200);
		gapHeight = parseInteger(gapHeight, 0, 255);
		return sendCommand('Q' + makeParams(height, gapHeight));
	}
	
	async function setPageWidth(width) {
		width = parseInteger(width, 10, 384);
		return sendCommand('q' + makeParams(width));
	}
	
	async function setOffset(x, y) {
		x = parseInteger(x, 0, 2047);
		y = parseInteger(y, 0, 2047);
		return sendCommand('R' + makeParams(x, y));
	}
	
	async function setFlipped(flipped) {
		return sendCommand('Z' + (flipped ? 'B' : 'T'));
	}
	
	async function setTime(month, day, year, hour, minute, second) {
		month = parseInteger(month, 1, 12);
		day = parseInteger(day, 1, 31);
		year = parseInteger(year, 0, 99);
		hour = parseInteger(hour, 0, 23);
		minute = parseInteger(minute, 0, 59);
		second = parseInteger(second, 0, 59);
		return sendCommand('TS' + makeParams(month, day, year, hour, minute, second));
	}
	
	function setDateFormat() {
		throw new Exception('Not implemented');
		return sendCommand('TTh:m:s');
	}
	
	function setTimeFormat() {
		throw new Exception('Not implemented');
		return sendCommand('TDdd-me-y4');
	}
	
	function defineCounter() {
		throw new Exception('Not implemented');
		return sendCommand('C0,6,R0,+1,"Counter 1: "');
	}
	
	function defineString() {
		throw new Exception('Not implemented');
		return sendCommand('V1,6,L,"Name: "');
	}
	
	function initializeStringsAndCounters() {
		throw new Exception('Not implemented');
		return sendCommand('?');
	}
	
	async function clearStringsAndCounters() {
		return sendCommand('VC');
	}
	
	function generateText() {
		throw new Exception('Not implemented');
		return sendCommand('A100,10,3,2,3,3,N,"This is only a Test"');
	}
	
	async function generateBarcode(x, y, rotation, type, thicknessNarrow, thicknessWide, height, includeText, data) {
		x = parseInteger(x, 0, 2047);
		y = parseInteger(y, 0, 2047);
		rotation = parseInteger(rotation, 0, 3);
		thicknessNarrow = parseInteger(thicknessNarrow, 1, 6);
		thicknessWide = parseInteger(thicknessWide, 2, 10);
		height = parseInteger(height, 24, 1000);
		return sendCommand('B' + makeParams(x, y, rotation, type, thicknessNarrow, thicknessWide, height, includeText ? 'B' : 'N', quoteString(data)));
	}
	
	function generateBarcode2D() {
		throw new Exception('Not implemented');
		return sendCommand('b0,0,P,384,280,"Data"');
	}
	
	function drawRectangle() {
		throw new Exception('Not implemented');
		return sendCommand('LO10,10,100,200');
	}
	
	function drawLine() {
		throw new Exception('Not implemented');
		return sendCommand('LSE10,10,8,100,200');
	}
	
	function drawFrame() {
		throw new Exception('Not implemented');
		return sendCommand('X100,10,5,300,200');
	}
	
	function drawImage() {
		throw new Exception('Not implemented');
		return sendCommand('GW100,10,5,300,.....');
	}
	
	async function beginForm(name) {
		return sendCommand('FS' + quoteString(name));
	}
	
	async function endForm(name) {
		return sendCommand('FE');
	}
	
	async function loadForm(name) {
		return sendCommand('FR' + quoteString(name));
	}
	
	async function deleteForm(name) {
		return sendCommand('FK' + quoteString(name));
	}
	
	async function listForm(name) {
		return sendCommand('FI' + (name ? quoteString(name) : ''));
	}
	
	async function beginWeightForm(name) {
		return sendCommand('SS' + quoteString(name));
	}
	
	async function endWeightForm(name) {
		return sendCommand('SE');
	}
	
	async function loadWeightForm(name) {
		return sendCommand('SR' + quoteString(name));
	}
	
	async function deleteWeightForm(name) {
		return sendCommand('SK' + quoteString(name));
	}
	
	async function listWeightForm(name) {
		return sendCommand('SI' + (name ? ('"' + name + '"') : ''));
	}
	
	async function beginGraphic(name, length) {
		length = parseInteger(length, 0, 65000);
		return sendCommand('GM' + makeParams(quoteString(name), length));
	}
	
	async function drawGraphic(x, y, name) {
		x = parseInteger(x, 0, 2047);
		y = parseInteger(y, 0, 2047);
		return sendCommand('GG' + makeParams(x, y, quoteString(name)));
	}
	
	async function deleteGraphic(name) {
		return sendCommand('GK' + quoteString(name));
	}
	
	async function listGraphics() {
		return sendCommand('GI');
	}
	
	function loadFont() {
		throw new Exception('Not implemented');
		return sendCommand('ES"Name"p1p2p3a1b1c1D1a2b2c2D2 ... anbncnDn');
	}
	
	async function deleteFont(name) {
		return sendCommand('EK' + quoteString(name));
	}
	
	async function listFonts() {
		return sendCommand('EI');
	}
	
	async function clearMemory() {
		return sendCommand('M');
	}
	
	async function clearBuffer() {
		return sendCommand('N');
	}
	
	async function reset() {
		return sendCommand('RESET');
	}
	
	async function printLabels(copies, batches) {
		copies = parseInteger(copies, 1, 1000, 1);
		batches = parseInteger(batches, 1, 1000);
		return sendCommand('P' + makeParams(batches, copies));
	}
	
	async function continuePrint() {
		return sendCommand('PC');
	}
	
	async function printText(text) {
		return sendCommand('=' + text);
	}
	
	async function printDiagnostics() {
		return sendCommand('U');
	}
	
	function getMemoryInfo() {
		throw new Exception('Not implemented');
		return sendCommand('UM');
	}
	
	function getWeightForms() {
		throw new Exception('Not implemented');
		return sendCommand('UF');
	}
	
	function getGraphics() {
		throw new Exception('Not implemented');
		return sendCommand('UG');
	}
	
	function getFonts() {
		throw new Exception('Not implemented');
		return sendCommand('UE');
	}
	
	function setSerialPortOptions() {
		throw new Exception('Not implemented');
		return sendCommand('Y19,O,7,1');
	}
	
	function chooseFont() {
		throw new Exception('Not implemented');
		return sendCommand('@2');
	}
	
	async function restart() {
		return sendCommand('^@');
	}
	
	async function resetDefaults() {
		return sendCommand('^default');
	}
	
	function getCountersAndVariables() {
		throw new Exception('Not implemented');
		return sendCommand('VI');
	}
	
	function getWeightFormName() {
		throw new Exception('Not implemented');
		return sendCommand('SA');
	}
	
	async function printWeightForm(count, copies) {
		count = parseInteger(count, 0, 1000);
		copies = parseInteger(copies, 0, 1000);
		return sendCommand('PA' + makeParams(count, copies));
	}
	
	async function enterDiagnosticsMode() {
		return sendCommand('dump');
	}
	
	function getPrinterVersion() {
		throw new Exception('Not implemented');
	}
	
	function disableReceivingData() {
		throw new Exception('Not implemented');
	}
	
	function enableReceivingData() {
		throw new Exception('Not implemented');
	}
	
	async function beep() {
		return send(String.fromCharCode(0x07));
	}
	
	async function deleteGraphicData() {
		return send(String.fromCharCode(0x17));
	}
	
	async function echo() {
		return send(String.fromCharCode(0x05));
	}
	
	function getRemainingLabels() {
		throw new Exception('Not implemented');
		return sendCommand('PI');
	}
	
	function setParameters() {
		throw new Exception('Not implemented');
		return sendCommand('sa,b,c,d');
	}
	
	function getDateTime() {
		throw new Exception('Not implemented');
		return sendCommand('TI');
	}
	
	function setDeviceId(id) {
		throw new Exception('Not implemented');
	}
	
	function disableReceivingDataById(id) {
		throw new Exception('Not implemented');
	}
	
	function enableReceivingDataById(id) {
		throw new Exception('Not implemented');
	}
	
	// Bluetooth module functions not implemented
	
	function enableReadOnly() {
		throw new Exception('Not implemented');
	}
	
	function disableReadOnly() {
		throw new Exception('Not implemented');
	}
	
	return {
		connect,
		disconnect,
		setDensity,
		setCodePage,
		setPageHeight,
		setPageWidth,
		setOffset,
		setFlipped,
		setTime,
		setDateFormat,
		setTimeFormat,
		defineCounter,
		defineString,
		initializeStringsAndCounters,
		clearStringsAndCounters,
		generateText,
		generateBarcode,
		generateBarcode2D,
		drawRectangle,
		drawLine,
		drawFrame,
		drawImage,
		beginForm,
		endForm,
		loadForm,
		deleteForm,
		listForm,
		beginWeightForm,
		endWeightForm,
		loadWeightForm,
		deleteWeightForm,
		listWeightForm,
		beginGraphic,
		drawGraphic,
		deleteGraphic,
		listGraphics,
		loadFont,
		deleteFont,
		listFonts,
		clearMemory,
		clearBuffer,
		reset,
		printLabels,
		continuePrint,
		printText,
		printDiagnostics,
		getMemoryInfo,
		getWeightForms,
		getGraphics,
		getFonts,
		setSerialPortOptions,
		chooseFont,
		restart,
		resetDefaults,
		getCountersAndVariables,
		getWeightFormName,
		printWeightForm,
		enterDiagnosticsMode,
		getPrinterVersion,
		disableReceivingData,
		enableReceivingData,
		beep,
		deleteGraphicData,
		echo,
		getRemainingLabels,
		setParameters,
		getDateTime,
		setDeviceId,
		disableReceivingDataById,
		enableReceivingDataById,
		enableReadOnly,
		disableReadOnly
	};
}
