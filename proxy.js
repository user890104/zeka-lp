const fs = require('fs');
const config = JSON.parse(fs.readFileSync('config.json'));
const http = require('http');
const HttpDispatcher = require('httpdispatcher');
const dispatcher = new HttpDispatcher();
const Printer = require('./printer');

const printer = new Printer({
	autoClose: true,
	port: config.serial.port,
	baudrate: config.serial.baudrate
});

dispatcher.onGet('/', function(req, res) {
    res.writeHead(200, {
		'Content-Type': 'text/html'
	});
    res.end('<h1>Hey, this is the homepage of your server</h1>');
});

const server = http.createServer(function(req, res) {
	dispatcher.dispatch(req, res);
});

server.listen(config.http.port, config.http.host, function() {
    console.log('Server listening on: http://%s:%s', config.http.host, config.http.port);
});

//printer.print('B24,24,0,E80,5,10,240,B,"0000001"\n');
