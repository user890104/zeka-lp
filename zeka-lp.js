var Printer = require('./printer');
var sharp = require('sharp');
var imageEncoder = require('./zeka-lp-image');

var printer = new Printer({
	autoClose: true
});

// Set width in dots
printer.print('q384');

// Set length in dots (8 dots = 1mm)
printer.print('Q304,0');

// print SVG
var input = 'initlab-logo.svg';

// print PNG
//var input = 'logo.png';
var image = sharp(input).negate().resize(384, 200);

//var image = sharp(input).negate().resize(384, 200); // max: 384x200

imageEncoder.imageToCommand(image, function(data) {
	printer.print(data);
});
