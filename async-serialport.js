'use strict';

const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')

module.exports = function(options) {
	options = options || {};
	
	const list = SerialPort.list;
	
	const serialport = new SerialPort(options.port || 'COM1', {
		autoOpen: false,
		baudRate: options.baudRate || 115200
	});
	
	function getBaudRate() {
		return serialport.baudRate;
	}
	
	function isOpen() {
		return serialport.isOpen;
	}
	
	function getPath() {
		return serialport.path;
	}
	
	async function promisify(context, fn, args) {
		if (!args) {
			args = [];
		}
		
		args = Array.from(args);
		
		return new Promise(function(resolve, reject) {
			args.push(function (err, data) {
				if (err) {
					return reject(err);
				}
				
				resolve(data);
			});
			fn.apply(context, args);
		});
	}
	
	async function open() {
		return promisify(serialport, serialport.open);
	}
	
	async function update(options) {
		return promisify(serialport, serialport.update, arguments);
	}
	
	async function write(data, encoding) {
		return promisify(serialport, serialport.write, arguments);
	}
	
	function read(number) {
		return serialport.read(number);
	}
	
	async function close() {
		return promisify(serialport, serialport.close);
	}
	
	async function set(options) {
		return promisify(serialport, serialport.set, arguments);
	}
	
	async function get() {
		return promisify(serialport, serialport.get);
	}
	
	async function flush() {
		return promisify(serialport, serialport.flush);
	}
	
	async function drain() {
		return promisify(serialport, serialport.drain);
	}
	
	const pause = serialport.pause;
	const resume = serialport.resume;	
	
	const parser = new Readline();
	serialport.pipe(parser);
	
	return {
		list,
		getBaudRate,
		isOpen,
		getPath,
		open,
		update,
		write,
		read,
		close,
		set,
		get,
		flush,
		drain,
		pause,
		resume
	};
}
