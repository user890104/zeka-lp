function buildCommand(x, y, w, h, data) {
	var prefix = new Buffer('GW' + x + ',' + y + ',' + w + ',' + h + ',');
	var suffix = new Buffer('\r\n');
	
	return Buffer.concat(
		[prefix, data, suffix],
		prefix.length + data.length + suffix.length
	);
}

function imageToArray(image, callback) {
	var result = [];
	
	image
		.grayscale()
		.raw()
		.toBuffer(function(err, buffer, info) {
			if (err) {
				console.error(err);
				return;
			}
			
			if (info.width % 8 !== 0) {
				throw new Exception('Width must be a multiple of 8');
			}
			
			if (info.width > 1016) {
				throw new Exception('Width cannot be more than 1016');
			}
			
			if (info.height > 400) {
				throw new Exception('Height cannot be more than 200');
			}
			
			var outputLength = (info.width / 8) * info.height;
			var i, j;
			
			for (i = 0; i < outputLength; ++i) {
				var start = i * 8;
				var piece = buffer.slice(start, start + 8);
				var currByte = 0;
				
				for (j = 0; j < 8; ++j) {
					if (piece[j] < 128) {
						currByte |= (1 << (7 - j));
					}
				}
				
				result.push(currByte);
			}
			
			callback(result, info);
		});
}

function imageToCommand(image, callback) {
	if (!image) {
		callback('');
		return;
	}
	imageToArray(image, function(data, info) {
		var buffer = new Buffer(data);
		var x = (384 - info.width) / 2;
		var y = (304 - info.height) / 2;
		var command = buildCommand(Math.floor(x), Math.floor(y), info.width / 8, info.height, buffer);
		callback(command);
	});
}

module.exports = {
	buildCommand: buildCommand,
	imageToArray: imageToArray,
	imageToCommand: imageToCommand,
};
